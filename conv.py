import tensorflow as tf
from tensorflow.python.keras.layers import Dense, Conv2D, Flatten, MaxPooling2D, Dropout


def get_conv(class_count=10, image_size=(128,128,3)):
    model = tf.keras.Sequential()
    model.add(Conv2D(32, kernel_size=(5, 5), strides=(1, 1),
                     activation='relu',
                     input_shape=image_size))
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
    model.add(Dropout(0.2))
    model.add(Conv2D(32, kernel_size=(5, 5), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
    model.add(Conv2D(64, (5, 5), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Flatten())
    model.add(Dense(100, activation='relu'))
    model.add(Dense(50, activation='relu'))
    model.add(Dense(class_count, activation='softmax'))

    model.compile(loss=tf.keras.losses.categorical_crossentropy,
                  # optimizer=tf.keras.optimizers.SGD(lr=0.01),
                  optimizer=tf.keras.optimizers.Adam(lr=1e-4),
                  metrics=['accuracy'])
    return model
