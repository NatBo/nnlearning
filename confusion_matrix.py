import conv
import data_loader
import nn
import numpy as np
import random
from sklearn.metrics import confusion_matrix
import tensorflow as tf


def result_print(classes_names, result_vec, target_vec):
    BOLD = '\033[1m'
    ENDC = '\033[0m'
    HEADER = '\033[95m'
    OKGREEN = '\033[92m'
    m = max(result_vec)
    for p, name, t in zip(result_vec, classes_names, target_vec):
        if p == m:
            print(BOLD,end="")
        if t > 0.5:
            print(OKGREEN,end="")
        print("\t %.8f" % p, name, ENDC)


classes_name, x_data, y_data = data_loader.get_learning_data()
print(classes_name)
model = conv.get_conv(len(classes_name))
# model = tf.keras.models.load_model("./models/simple2.h5py")
x_train = []
y_train = []
x_test = []
y_test = []
for i, o in zip(x_data, y_data):
    if random.random() > 0.1:
        x_train.append(i)
        y_train.append(o)
    else:
        x_test.append(i)
        y_test.append(o)

x_train = np.array(x_train)
y_train = np.array(y_train)
x_test = np.array(x_test)
y_test = np.array(y_test)

print("Done", len(x_train), len(y_train), len(x_test), len(y_test))
print(x_train.shape, y_train.shape, x_test.shape, y_test.shape)
print(x_train[0].shape, y_train[0].shape, x_test[0].shape, y_test[0].shape)

model.fit(np.array(x_train), np.array(y_train),
          batch_size=32,
          epochs=20,
          verbose=2,
          validation_data=(np.array(x_test), np.array(y_test)),
          )

model.save("./models/simple2.h5py")

result = nn.predict(model, x_test)

print(confusion_matrix(np.argmax(np.array(result), axis=1), np.argmax(np.array(y_test), axis=1)))
# for x, y, r in zip(x_data[-15:],y_data[-15:],result):
#     print()
#     result_print(classes_name, r, y)
# model = nn.create_and_learn_nn(learning_data)
# model = tf.keras.models.load_model('model.h5py')
# model.load_weights('model_weights.h5py')

# testing_data = data_loader.get_prediction_data("./datasets/set_2")
# nn.predict(model, testing_data, classes_name)