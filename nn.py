import random

import cv2
import numpy as np
import tensorflow as tf


def learn(model, inputs, outputs, validation_prop=0.1):
    x_train = []
    y_train = []
    x_test = []
    y_test = []
    for i, o in zip(inputs, outputs):
        if random.random() > validation_prop:
            x_train.append(i)
            y_train.append(o)
        else:
            x_test.append(i)
            y_test.append(o)

    x_train = np.array(x_train)
    y_train = np.array(y_train)
    x_test = np.array(x_test)
    y_test = np.array(y_test)

    print("Done", len(x_train), len(y_train), len(x_test), len(y_test))
    print(x_train.shape, y_train.shape, x_test.shape, y_test.shape)
    print(x_train[0].shape, y_train[0].shape, x_test[0].shape, y_test[0].shape)

    model.fit(np.array(x_train), np.array(y_train),
              batch_size=32,
              epochs=20,
              verbose=2,
              validation_data=(np.array(x_test), np.array(y_test)),
              )

    return model


def predict(model, images):
    result = model.predict(images)
    return result