import tensorflow as tf
from tensorflow.python.keras.layers import Dense, Conv2D, Flatten, MaxPooling2D


def create_and_learn_nn(data, path_model='./model.h5py', path_weight='./model_weights.h5py'):
    classes_name, x_train, y_train, x_test, y_test = data
    model = tf.keras.Sequential()
    model.add(Conv2D(32, kernel_size=(5, 5), strides=(1, 1),
                     activation='relu',
                     input_shape=(128, 128, 3)))
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
    model.add(Conv2D(32, kernel_size=(5, 5), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
    model.add(Conv2D(64, (5, 5), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Flatten())
    model.add(Dense(1000, activation='relu'))
    model.add(Dense(500, activation='relu'))
    model.add(Dense(len(classes_name), activation='softmax'))

    model.compile(loss=tf.keras.losses.categorical_crossentropy,
                  optimizer=tf.keras.optimizers.SGD(lr=0.01),
                  metrics=['accuracy'])
    model.save(path_model)
    print("Model done")

    model.fit(x_train, y_train,
              batch_size=64,
              epochs=50,
              verbose=2,
              validation_data=(x_test, y_test),
              )
    model.save_weights(path_weight)
    return model


def predict(model, data, classes_name):
    images, names = data
    result = model.predict(images)

    BOLD = '\033[1m'
    ENDC = '\033[0m'
    HEADER = '\033[95m'
    for r, t in zip(result, names):
        m = max(r)
        print(HEADER, t, ENDC)
        for p, name in zip(r, classes_name):
            if p == m:
                print("\t", BOLD, "%.8f" % p, name, ENDC)
            else:
                print("\t", "%.8f" % p, name)
