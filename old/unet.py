import cv2
import numpy as np
from random import shuffle
import random
import tensorflow as tf
from glob import glob
from tensorflow.python.keras.layers import UpSampling2D, Dense, Conv2D, Flatten, Dropout, MaxPooling2D, Input, \
    Concatenate


# create model
# model = tf.keras.Sequential()
# model.add(Input(shape=(3, None, None)))
# model.add(Conv2D(64, 3, activation='relu', padding='same', kernel_initializer='he_normal'))
# model.add(Conv2D(64, 3, activation='relu', padding='same', kernel_initializer='he_normal'))
# model.add(MaxPooling2D(pool_size=(2, 2)))
#
# model.add(Conv2D(128, 3, activation='relu', padding='same', kernel_initializer='he_normal'))
# model.add(Conv2D(128, 3, activation='relu', padding='same', kernel_initializer='he_normal'))
# model.add(MaxPooling2D(pool_size=(2, 2)))
#
# model.add(Conv2D(256, 3, activation='relu', padding='same', kernel_initializer='he_normal'))
# model.add(Conv2D(256, 3, activation='relu', padding='same', kernel_initializer='he_normal'))
# model.add(MaxPooling2D(pool_size=(2, 2)))
#
# model.add(Conv2D(512, 3, activation='relu', padding='same', kernel_initializer='he_normal'))
# model.add(Conv2D(512, 3, activation='relu', padding='same', kernel_initializer='he_normal'))
# model.add(Dropout(0.5))
# model.add(MaxPooling2D(pool_size=(2, 2)))
#
# model.add(Conv2D(1024, 3, activation='relu', padding='same', kernel_initializer='he_normal'))
# model.add(Conv2D(1024, 3, activation='relu', padding='same', kernel_initializer='he_normal'))
# model.add(Dropout(0.5))
#
# model.add(UpSampling2D(size=(2,2)))
# model.add(Conv2D(512, 2, activation='relu', padding='same', kernel_initializer='he_normal'))
# model.add(Conv2D(512, 2, activation='relu', padding='same', kernel_initializer='he_normal'))

def create_model():
    inputs = Input((None, None, 3))

    conv1 = Conv2D(64, 3, activation='relu', padding='same', kernel_initializer='he_normal')(inputs)
    conv1 = Conv2D(64, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv1)
    pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)

    conv2 = Conv2D(128, 3, activation='relu', padding='same', kernel_initializer='he_normal')(pool1)
    conv2 = Conv2D(128, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv2)
    pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)

    conv3 = Conv2D(256, 3, activation='relu', padding='same', kernel_initializer='he_normal')(pool2)
    conv3 = Conv2D(256, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv3)
    pool3 = MaxPooling2D(pool_size=(2, 2))(conv3)

    conv4 = Conv2D(512, 3, activation='relu', padding='same', kernel_initializer='he_normal')(pool3)
    conv4 = Conv2D(512, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv4)
    drop4 = Dropout(0.5)(conv4)
    pool4 = MaxPooling2D(pool_size=(2, 2))(drop4)

    conv5 = Conv2D(1024, 3, activation='relu', padding='same', kernel_initializer='he_normal')(pool4)
    conv5 = Conv2D(1024, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv5)
    drop5 = Dropout(0.5)(conv5)

    up6 = Conv2D(512, 2, activation='relu', padding='same', kernel_initializer='he_normal')(
        UpSampling2D(size=(2, 2))(drop5))
    con = Concatenate(axis=3)
    merge6 = con([drop4, up6])
    conv6 = Conv2D(512, 3, activation='relu', padding='same', kernel_initializer='he_normal')(merge6)
    conv6 = Conv2D(512, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv6)

    up7 = Conv2D(256, 2, activation='relu', padding='same', kernel_initializer='he_normal')(
        UpSampling2D(size=(2, 2))(conv6))
    con = Concatenate(axis=3)
    merge7 = con([conv3, up7])
    conv7 = Conv2D(256, 3, activation='relu', padding='same', kernel_initializer='he_normal')(merge7)
    conv7 = Conv2D(256, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv7)

    up8 = Conv2D(128, 2, activation='relu', padding='same', kernel_initializer='he_normal')(
        UpSampling2D(size=(2, 2))(conv7))
    con = Concatenate(axis=3)
    merge8 = con([conv2, up8])
    conv8 = Conv2D(128, 3, activation='relu', padding='same', kernel_initializer='he_normal')(merge8)
    conv8 = Conv2D(128, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv8)

    up9 = Conv2D(64, 2, activation='relu', padding='same', kernel_initializer='he_normal')(
        UpSampling2D(size=(2, 2))(conv8))
    con = Concatenate(axis=3)
    merge9 = con([conv1, up9])
    conv9 = Conv2D(64, 3, activation='relu', padding='same', kernel_initializer='he_normal')(merge9)
    conv9 = Conv2D(64, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv9)
    conv9 = Conv2D(2, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv9)
    conv10 = Conv2D(2, 1, activation='sigmoid')(conv9)

    model = tf.keras.models.Model(inputs=inputs, outputs=conv10)
    model.compile(loss=tf.keras.losses.binary_crossentropy,
                  # optimizer=tf.keras.optimizers.SGD(lr=0.01),
                  optimizer=tf.keras.optimizers.Adam(lr=1e-4),
                  metrics=['accuracy'])

    model.save("unet.h5py")


def load_model_and_learn_it():
    model = tf.keras.models.load_model("unet.h5py")
    # tmp = model.get_layer(index=-1)
    print("model loaded")
    x_train = []
    y_train = []
    x_test = []
    y_test = []
    var = glob("./datasets/set_2/*.jpg")
    shuffle(var)
    for path in var[:500]:
        # print(path)
        img_input = cv2.imread(path, 0)
        if img_input is None:
            print("SKIP 1", path)
            continue
        img_input = cv2.resize(img_input, (120, 120))
        img_input = img_input / 255.0
        map_path = path.replace("set_2", "set_2-trimap").replace("jpg", "png")
        img_target = cv2.imread(map_path, 0)
        _, img_target = cv2.threshold(img_target, 1, 255, cv2.THRESH_BINARY_INV)
        # img_target = cv2.merge((img_target, 1 - img_target))
        img_target = cv2.resize(img_target, (120, 120))
        img_input = np.reshape(img_input, (120, 120, 1))
        img_target = np.reshape(img_target, (120, 120, 1))
        # cv2.imshow("cokolwiek1", img_input)
        # cv2.imshow("cokolwiek2", img_target)
        # cv2.waitKey()

        if img_target is None or img_input is None:
            print("SKIP 2", path)
            continue

        if random.random() < 0.9:
            x_train.append(img_input)
            y_train.append(img_target)
        else:
            x_test.append(img_input)
            y_test.append(img_target)

    x_train = np.array(x_train)
    y_train = np.array(y_train)
    x_test = np.array(x_test)
    y_test = np.array(y_test)

    print("Done", len(x_train), len(y_train), len(x_test), len(y_test))
    print(x_train.shape, y_train.shape, x_test.shape, y_test.shape)
    print(x_train[0].shape, y_train[0].shape, x_test[0].shape, y_test[0].shape)

    model.fit(np.array(x_train), np.array(y_train),
              batch_size=64,
              epochs=50,
              verbose=2,
              validation_data=(np.array(x_test), np.array(y_test)),
              )

    model.save_weights("unet_weights.h5py")
    # cv2.imshow("cokolwiek", x[5])
    # cv2.imshow("cokolwiek2", y[5])
    # cv2.waitKey()


def predict():
    model = tf.keras.models.load_model('unet.h5py')
    model.load_weights('unet_weights.h5py')

    print("model loaded")
    x_train = []
    y_train = []
    var = glob("./datasets/set_2/*.jpg")
    shuffle(var)
    for path in var[:10]:
        print(path)
        img_input = cv2.imread(path)
        img_input = cv2.resize(img_input, (120, 120))
        map_path = path.replace("set_2", "set_2-trimap").replace("jpg", "png")
        img_target = cv2.imread(map_path, 0)
        _, img_target = cv2.threshold(img_target, 1, 255, cv2.THRESH_BINARY_INV)
        img_target = cv2.merge((img_target, 1 - img_target))
        img_target = cv2.resize(img_target, (120, 120))

        x_train.append(img_input)
        y_train.append(img_target)

    x_train = np.array(x_train)
    y_train = np.array(y_train)
    result = model.predict(x_train)

    for r, i, t in zip(result, x_train, y_train):
        cv2.imshow("result", cv2.split(r)[1] * 128)
        cv2.imshow("input", i)
        cv2.imshow("target", cv2.split(t)[0] * 128)
        cv2.waitKey()


# create_model()
load_model_and_learn_it()

# predict()
