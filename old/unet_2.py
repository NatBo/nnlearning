import cv2
import numpy as np
from random import shuffle
import random
import tensorflow as tf
from glob import glob
from tensorflow.python.keras.layers import UpSampling2D, Dense, Conv2D, Flatten, Dropout, MaxPooling2D, Input, \
    Concatenate
from tensorflow.python.layers.convolutional import Conv3D


def get_unet(n_ch,patch_height,patch_width):
    inputs = Input(shape=(patch_height, patch_width, 1))
    conv1 = Conv2D(32, (3, 3), activation='relu', padding='same')(inputs)
    conv1 = Dropout(0.2)(conv1)
    conv1 = Conv2D(32, (3, 3), activation='relu', padding='same')(conv1)
    pool1 = MaxPooling2D((2, 2))(conv1)
    #
    conv2 = Conv2D(64, (3, 3), activation='relu', padding='same')(pool1)
    conv2 = Dropout(0.2)(conv2)
    conv2 = Conv2D(64, (3, 3), activation='relu', padding='same')(conv2)
    pool2 = MaxPooling2D((2, 2))(conv2)
    #
    conv3 = Conv2D(128, (3, 3), activation='relu', padding='same')(pool2)
    conv3 = Dropout(0.2)(conv3)
    conv3 = Conv2D(128, (3, 3), activation='relu', padding='same')(conv3)

    up1 = UpSampling2D(size=(2, 2))(conv3)
    con1 = Concatenate(axis=3)
    up1 = con1([conv2,up1])
    conv4 = Conv2D(64, (3, 3), activation='relu', padding='same')(up1)
    conv4 = Dropout(0.2)(conv4)
    conv4 = Conv2D(64, (3, 3), activation='relu', padding='same')(conv4)
    #
    up2 = UpSampling2D(size=(2, 2))(conv4)
    con2 = Concatenate(axis=3)
    up2 = con2([conv1, up2])
    conv5 = Conv2D(32, (3, 3), activation='relu', padding='same')(up2)
    conv5 = Dropout(0.2)(conv5)
    conv5 = Conv2D(32, (3, 3), activation='relu', padding='same')(conv5)
    #
    conv6 = Conv2D(1, (1, 1), activation='relu',padding='same')(conv5)
    ############
    model = tf.keras.models.Model(inputs=inputs, outputs=conv6)
    model.compile(loss=tf.keras.losses.binary_crossentropy,
                  # optimizer=tf.keras.optimizers.SGD(lr=0.01),
                  optimizer=tf.keras.optimizers.Adam(lr=1e-4),
                  metrics=['accuracy'])

    return model

model = get_unet(1, 120,120)
model.save("unet.h5py")