from icrawler.builtin import GoogleImageCrawler
import os

classes = [['husky dog', 'husky'],
           ['bernard dog', 'bernardyn'],
           ['dalmatian dog', 'dalmatynczyk'],
           ['golden retriever dog', 'golden retriever'],
           ['german sheepard dog', 'owczarek niemiecki'],
           ['rottweiler dog', 'rottweiler'],
           ['shih tzu dog', 'shih tzu'],
           ['smooth dachshund', 'jamnik krotkowlosy']
           ]
set_name = "set_1"

for keyword, name in classes:
    google_crawler = GoogleImageCrawler(storage={'root_dir': os.path.join("./datasets", set_name, name)})
    google_crawler.crawl(keyword=keyword, max_num=100)
