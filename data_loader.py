import glob
import random
import cv2
import os

import numpy as np

data_set_path = "./datasets/set_3"
avg_path = "avg.npy"
std_path = "std.npy"
img_size =(128,128)


def calculate_avg(images):
    result = np.zeros(images[0].shape)
    for i in images:
        result += i
    result /= len(images)

    np.save(avg_path, result)
    # cv2.imwrite("avg.png", result.astype(np.int))


def calculate_std(images):
    avg = np.load(avg_path)
    result = np.zeros(images[0].shape)
    for i in images:
        result += (i - avg) ** 2
    result /= len(images) - 1
    result = np.sqrt(result)

    np.save(std_path, result)
    # cv2.imwrite("std.png", result.astype(np.int))


def prepare_image(img_path, std, avg):
    """
    Loading images and rescaling to size
    :param avg:
    :param std:
    :param img_path: path to images
    :param size: target size in pixel
    :return: rescaling image
    """
    img = cv2.imread(img_path)
    if img is None:
        return None
    return (cv2.resize(img, img_size) - avg) / std


def get_learning_data(count=None):
    """
    Load images.
    :return: 3 lists: classes name, images set X, Y
    """

    classes_name = sorted(os.listdir(data_set_path))
    num_classes = len(classes_name)
    x, y = [], []
    std = np.load(std_path)
    avg = np.load(avg_path)
    for i, class_name in enumerate(classes_name):
        for img_path in glob.glob(os.path.join(data_set_path, class_name, "*")):
            img = prepare_image(img_path, std, avg)
            if img is None:
                print("Can't load:", img_path)
                continue
            x.append(img)
            output = np.zeros(num_classes)
            output[i] = 1.0
            y.append(output)
    data = list(zip(x, y))
    random.shuffle(data)
    x = np.array([a[0] for a in data])
    y = np.array([a[1] for a in data])
    if count is None:
        count = x.shape[0]
    return classes_name, x[:count], y[:count]


if __name__ == "__main__":
    print("calculate avg and std...:")
    images = []
    for p in glob.glob("./datasets/set_3/*/*"):
        images.append(cv2.resize(cv2.imread(p), img_size))
    calculate_avg(images)
    calculate_std(images)
