import glob
import re
import xml.parsers
import xml.etree.ElementTree
import cv2
import os
import ntpath

# for path in glob.glob("./datasets/set2-xml/*.xml"):
#     x = xml.etree.ElementTree.parse(path)
#     if(x.find("./object/name").text == "cat"):
#         continue
#     image_name = x.find("./filename").text
#     img = cv2.imread("./datasets/set_2/"+image_name)
#     x_min, y_min, x_max, y_max = int(x.find("./object/bndbox/xmin").text), int(x.find("./object/bndbox/ymin").text), int(x.find("./object/bndbox/xmax").text), int(x.find("./object/bndbox/ymax").text)
#     img = img[y_min:y_max, x_min:x_max]
#     cv2.imwrite("./datasets/set2-ROI/"+image_name, img)

# classes = dict()
# for name in glob.glob("./datasets/set2-ROI/*"):
#     result = re.sub("[\d]*", "", name.split("/")[-1].split("\\")[-1].replace("_", " ").replace(".jpg", ""))
#     classes[result] = 0
#
# for i, c in enumerate(classes):
#     print(c, i)

dirs = ["american bulldog",
"american pit bull terrier",
"basset hound",
"beagle",
"boxer",
"chihuahua",
"english cocker spaniel",
"english setter",
"german shorthaired",
"great pyrenees",
"havanese",
"japanese chin",
"keeshond",
"leonberger",
"miniature pinscher",
"newfoundland",
"pomeranian",
"pug",
"saint bernard",
"samoyed",
"scottish terrier",
"shiba inu",
"staffordshire bull terrier",
"wheaten terrier",
"yorkshire terrier"]

for d in dirs:
    for t in glob.glob("./datasets/set2-ROI/"+d.replace(" ", "_")+"*"):
        os.rename(t, "./datasets/set_3/"+d+"/"+ntpath.basename(t))