import conv
import data_loader
import nn
import tensorflow as tf


def result_print(classes_names, result_vec, target_vec):
    BOLD = '\033[1m'
    ENDC = '\033[0m'
    HEADER = '\033[95m'
    OKGREEN = '\033[92m'
    m = max(result_vec)
    for p, name, t in zip(result_vec, classes_names, target_vec):
        if p == m:
            print(BOLD,end="")
        if t > 0.5:
            print(OKGREEN,end="")
        print("\t %.8f" % p, name, ENDC)


classes_name, x_data, y_data = data_loader.get_learning_data()
print(classes_name)
model = conv.get_conv(len(classes_name))
# model = tf.keras.models.load_model("./models/simple.h5py")
nn.learn(model, x_data[:-15],y_data[:-15])
model.save("./models/simple2.h5py")

# result = nn.predict(model, x_data[-15:])
# for x, y, r in zip(x_data[-15:],y_data[-15:],result):
#     print()
#     result_print(classes_name, r, y)
# model = nn.create_and_learn_nn(learning_data)
# model = tf.keras.models.load_model('model.h5py')
# model.load_weights('model_weights.h5py')

# testing_data = data_loader.get_prediction_data("./datasets/set_2")
# nn.predict(model, testing_data, classes_name)