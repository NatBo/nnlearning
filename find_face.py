import cv2


def find_face(img):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    detector = cv2.CascadeClassifier("./models/haarcascade_frontalcatface_extended.xml")
    faces = detector.detectMultiScale(gray, scaleFactor=8)
    return faces


def draw_roi(img, faces):
    for (x, y, w, h) in faces:
        img = cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)

    cv2.imshow('img', img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


if __name__ == "__main__":
    img = cv2.imread("datasets/set_2/Abyssinian_15.jpg")
    faces = find_face(img)
    draw_roi(img, faces)
